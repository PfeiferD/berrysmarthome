package de.dpfeifer.home;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.dvdme.ForecastIOLib.ForecastIO;

import de.dpfeifer.ForeCastConfig;
import io.socket.client.IO;
import io.socket.client.*;
import io.socket.emitter.*;

@Controller
public class HomeController {

	private ForecastIO forecastQueryConfig;
	
	@Autowired
	public HomeController(ForecastIO forecastQueryConfig) {
		this.forecastQueryConfig = forecastQueryConfig;
	}
	@RequestMapping("/home")
	public String home(Model model) {
	
		forecastQueryConfig.
	/*	Socket socket;
		try {
			System.out.println("SOCKET");
			socket = IO.socket("http://berry:8080/?username=admin&Password=Afyoerx2PM");
		
			socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

				@Override
				public void call(Object... args) {
					System.out.println("CONNECT");
					socket.disconnect();
				}

			}).on("deviceAttributeChanged", new Emitter.Listener() {

				@Override
				public void call(Object... args) {
					System.out.println("EVENT"+args.toString());
				}

			}).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

				@Override
				public void call(Object... args) {
					System.out.println("Disconnect");
				}

			});
			socket.connect();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		model.addAttribute("title", "Home - BerrySmathome");
		return "home";
	}
}
