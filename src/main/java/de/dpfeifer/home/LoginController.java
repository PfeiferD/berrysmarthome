package de.dpfeifer.home;


import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



@Controller
public class LoginController {
	@RequestMapping(value = {"", "/", "/login"})	
	public String login(Model model) {

		model.addAttribute("title","Login : BerrySmartHome");
		
		return "login";
	}
	
	public HttpHeaders createHeaders( String username, String password ){
		   return new HttpHeaders(){
		      {
		         String auth = username + ":" + password;
		         byte[] encodedAuth = Base64.encodeBase64( 
		            auth.getBytes(Charset.forName("US-ASCII")) );
		         String authHeader = "Basic " + new String( encodedAuth );
		         set( "Authorization", authHeader );
		      }
		   };
		}
	/**
	 * Controller für: POST /login/auth
	 * 
	 * Login-Form wurde abgeschickt, Daten überprüfen und Session erstellen
	 * 
	 * @param username
	 * @param password
	 * @param response
	 * @param redirectAttr	
	 * 
	 * @return	redirect:/user -oder- redirect:/login
	 */
	@RequestMapping(value="/login/auth",method = RequestMethod.POST)
	public String auth(@RequestParam("username") String username, @RequestParam("password") String password, HttpServletResponse response, RedirectAttributes redirectAttr){
		// Prüfen, ob valide
		if (username.isEmpty() || password.isEmpty() || username.equals(" ") || password.equals(" ")) {
			redirectAttr.addFlashAttribute("error","Bitte einen Benutzername und ein Passwort eingeben");
			return "redirect:/login";
		}
		final String uri = "http://berry:8080/api/config?password="+password;
		
		 RestTemplate restTemplate = new RestTemplate();

		 HttpEntity<String> request = new HttpEntity<String>(createHeaders(username, password));
		 try{
			 ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);
			 System.out.println(result);
		
			 JSONObject obj = new JSONObject(result.getBody());
			 JSONArray users = obj.getJSONObject("config").getJSONArray(("users"));
			 
			
			  
			 for(int i=0;i<users.length();i++){
				 JSONObject user = (JSONObject) users.get(i);
				if(user.get("username").equals(username) && user.get("password").equals(password)){
					return "redirect:/home";	
				}
				 
			 }
			
		 }catch(HttpServerErrorException ex){
			 System.out.println(ex);
		 }
		// Fehlerhafte Login-Daten
					redirectAttr.addFlashAttribute("error","Benutzername oder Passwort falsch");
					return "redirect:/login";	
	}
}
