package de.dpfeifer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.TemplateEngine;
@SpringBootApplication
public class BerrysmarthomeApplication {
	private static TemplateEngine templateEngine;
	public static String configPimatic;
	
	public static void main(String[] args) {
		SpringApplication.run(BerrysmarthomeApplication.class, args);
		
	}
	static {
        initializeTemplateEngine();
        initializePimaticConfig();
    }
	private static void initializePimaticConfig(){
		String password="Afyoerx2PM";
	
		 RestTemplate restTemplate = new RestTemplate();
		 final String uri = "http://berry:8080/api/config?password="+password;
			
		 
		 try{
			 String result = restTemplate.getForObject(uri, String.class);
			 System.out.println(result);
			 configPimatic = result;
		
		 }catch(RestClientException ex){
			 ex.printStackTrace();
		 }
		
		
		
	}
    private static void initializeTemplateEngine() {	        
        ServletContextTemplateResolver templateResolver = 
            new ServletContextTemplateResolver();
        // XHTML is the default mode, but we set it anyway for better understanding of code
        templateResolver.setTemplateMode("HTML5");
        // This will convert "home" to "/WEB-INF/templates/home.html"
        templateResolver.setPrefix("/berrysmarthome/resources/templates/");
        templateResolver.setSuffix(".html");
        // Template cache TTL=1h. If not set, entries would be cached until expelled by LRU
        templateResolver.setCacheTTLMs(3600000L);
        
        templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);	        
    }
}
