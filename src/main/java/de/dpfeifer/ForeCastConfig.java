package de.dpfeifer;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.dvdme.ForecastIOLib.ForecastIO;

@Configuration
public class ForeCastConfig  {
	
	@Bean(name = "forecastQueryConfig")
	public ForecastIO getForecastConfig() {
		ForecastIO foreCast = new ForecastIO("6bac823e459d0afda15688cbde24aa7b"); //instantiate the class with the API key. 
		foreCast.setUnits(ForecastIO.UNITS_SI);       
		foreCast.setLang(ForecastIO.LANG_GERMAN);            //sets the units as SI - optional
		foreCast.setExcludeURL("hourly,minutely");   //excluded the minutely and hourly reports from the reply
		return foreCast;
	}

}
